﻿using System;

namespace Calculator
{

    class Program
    {

        static void beispiel1()
        {
            Zahl a = new Zahl(Zustand.Normal, 2048);
            Zahl b = new Zahl(Zustand.Normal, 16);

            Console.WriteLine("a.istDefiniert: " + a.istDefiniert());
            Console.WriteLine("b.istDefiniert: " + b.istDefiniert());

            Console.WriteLine("a: " + a.ToString());
            Console.WriteLine("b: " + b.ToString());

            Console.WriteLine("a - b: " + Zahl.subtraktion(a, b).ToString());
        }


        static void beispiel2()
        {
            Zahl a = new Zahl(Zustand.Normal, 123);
            Zahl b = new Zahl(Zustand.Undefiniert);

            Console.WriteLine("a.istDefiniert: " + a.istDefiniert());
            Console.WriteLine("b.istDefiniert: " + b.istDefiniert());

            Console.WriteLine("a: " + a.ToString());
            Console.WriteLine("b: " + b.ToString());

            Console.WriteLine("a - b: " + Zahl.subtraktion(a, b).ToString());
        }




        static void Main(string[] args) {

            beispiel2();
            Console.WriteLine();
            Console.WriteLine("---------------------------");
        }
    }
}
