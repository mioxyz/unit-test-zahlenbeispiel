﻿using System;

namespace Calculator
{
	public enum Zustand
	{
		Undefiniert,
		Normal
	}

	public class Zahl
	{

		Zustand zustand = Zustand.Normal;
		public double wert;

		public Zahl(Zustand zustand = Zustand.Normal, double wert = 0)
		{
			this.wert = wert;
			this.zustand = zustand;
		}

		public bool istDefiniert() => (zustand == Zustand.Normal);

		public bool istUndefiniert() => !istDefiniert();

		override public string ToString()
		{
			if (zustand == Zustand.Undefiniert) return "Undefiniert";
			return wert.ToString();
		}
		public static Zahl addition(Zahl a, Zahl b)
		{
			if (a.istUndefiniert() || b.istUndefiniert())
			{
				return new Zahl(Zustand.Undefiniert);
			}

			return new Zahl(Zustand.Normal, a.wert + b.wert);
		}

		public static Zahl subtraktion(Zahl a, Zahl b)
		{
			if (a.istUndefiniert() || b.istUndefiniert())
			{
				return new Zahl(Zustand.Undefiniert);
			}

			return new Zahl(Zustand.Normal, a.wert - b.wert);
		}

		public static Zahl multiplikation(Zahl a, Zahl b)
		{
			if (a.istUndefiniert() || b.istUndefiniert())
			{
				return new Zahl(Zustand.Undefiniert);
			}

			return new Zahl(Zustand.Normal, a.wert * b.wert);

		}

		public static Zahl division(Zahl a, Zahl b)
		{
			if (a.istUndefiniert() && b.istUndefiniert())
			{
				return new Zahl(Zustand.Undefiniert);
			}

			if (a.wert == 0 && b.wert == 0)
			{
				return new Zahl(Zustand.Normal, 0);
			}

			if (b.wert == 0)
			{
				return new Zahl(Zustand.Undefiniert);
			}

			return new Zahl(Zustand.Normal, a.wert / b.wert);
		}

	}

}